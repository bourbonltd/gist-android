package build.gist.data.model

data class UserMessages(
    val topics: List<String>
)